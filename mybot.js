function new_game() {
}

var DIRECTIONS = [NORTH, EAST, SOUTH, WEST, TAKE, PASS];
var ITERATIONS = 10;
var MAXDEPTH = 5;
var GAMMA = 0.8;
var ALPHA = 0.5;

var qVals = new Array();
for (var i in DIRECTIONS) { 
  qVals[i] = new Array();
}

function makeFruitArray(toggle) {
  fruitArray = new Array();
  if (toggle == 0) {
    for (var i = 1; i <= get_number_of_item_types(); i++) {
      fruitArray.push(get_my_item_count(i));
    }
  } else {
      for (var i = 1; i <= get_number_of_item_types(); i++) {
        fruitArray.push(get_my_item_count(i));
      }
  }   
  return fruitArray;
}
    

function make_move() {
  var board = get_board();
  currentState = new GameState(board, get_my_x(), get_my_y(), get_opponent_x(), get_opponent_y(), makeFruitArray(0), makeFruitArray(1));
  for (var k = 0; k < ITERATIONS; k++) {
    var fringe = [currentState];
    var j = 0;
    while (fringe.length> 0 && j < MAXDEPTH) {
      var s = fringe.shift();
      //console.log(s.toString());
      for (var i in DIRECTIONS) {
        for (var x in DIRECTIONS) {
          var newS = change_state(s, i, x);
          var r = reward(s, newS);
          var maxActVal = 0;
          for (var f in DIRECTIONS) {
            var val = qVals[f][newS];
            if (val == undefined) { continue; }
            if (val > maxActVal) { maxActVal = val; }
          }
          var sample = r + (GAMMA*maxActVal)
          var oldQ = qVals[i][s]
          if (oldQ != undefined) {
            qVals[i][s] = ((1-ALPHA)*oldQ) + ALPHA*sample;
          } else { qVals[i][s] = ALPHA*sample }
          fringe.push(newS);
        }
      }
      j++;
    }
  }
  var bestAction = null;
  var bestActVal = -999999;
  for (act in DIRECTIONS) {
    if (act == NORTH) { console.log("NORTH"); }
    var val = qVals[act][currentState];
    if (val > bestActVal) {
      bestAction = act;
      bestActVal = val;
    }
  }
  //console.log(bestAction);
  return bestAction;
}

function change_state(state, myA, opA) {
  if (myA == TAKE) { 
    var field = state.board[state.myX][state.myY];
    var fruit = has_item(field);
    if (fruit) { state.myFruitArray[fruit] += 1; }
    state.board[state.myX][state.myY] = 0; 
  }
  if (opA == TAKE) { 
    var field = state.board[state.opX][state.opY];
    var fruit = has_item(field);
    if (fruit) { state.opFruitArray[fruit] += 1; }
    state.board[state.opX][state.opY] = 0; 
  }
  if (myA == EAST) { state.myX++; }
  if (opA == EAST) { state.opX++; }
  if (myA == NORTH) { state.myY++; }
  if (opA == NORTH) { state.opY++; }
  if (myA == WEST) { state.myX--; }
  if (opA == WEST) {state.opX--; }
  if (myA == SOUTH) {state.myY--; }
  if (opA == SOUTH) {state.opY--; }
  return state;
}

function GameState(board, myX, myY, opX, opY, myFruitArray, opFruitArray) {
  this.board = board;
  this.myX = myX;
  this.myY = myY;
  this.opX = opX;
  this.opY = opY;
  this.myFruitArray = myFruitArray;
  this.opFruitArray = opFruitArray;
}

function evaluateState(state) {
  var sum = 0;
  for (var i=1; i<=get_number_of_item_types(); i++) {
    if (state.myFruitArray[i] > state.opFruitArray[i]) { sum += 1; }
    if (state.myFruitArray[i] < state.opFruitArray[i]) { sum -= 1; }
  }
  return sum;
}

function reward(state, newState) {
  var result = 0;
  var stateVal = evaluateState(state);
  var newStateVal = evaluateState(newState);
  if (stateVal < newStateVal) { result = 1; }
  if (stateVal > newStateVal) { result = -1; }
  //console.log(result);
  return result;
}

// Optionally include this function if you'd like to always reset to a 
// certain board number/layout. This is useful for repeatedly testing your
// bot(s) against known positions.
//
//function default_board_number() {
//    return 123;
//}
